#!/usr/bin/env python

# When mailboxes are altered externally (eg via webmail), mutt marks its window as urgent
# forcing the i3 status bar visible and showing the window indicator as red

# This behaviour is unwanted, so this script watches for the mutt window being marked as urgent (via i3ipc and the WINDOW event) and unmarks it after a few seconds

import fcntl
import os
import sys

# depends on i3ipc-python (pip3 install i3ipc --user)
from i3ipc import Connection, Event


def instance_already_running(label="i3ListenWindowUrgent"):
    """
    Detect if an an instance with the label is already running, globally
    at the operating system level.

    Using `os.open` ensures that the file pointer won't be closed
    by Python's garbage collector after the function's scope is exited.

    The lock will be released when the program exits, or could be
    released if the file pointer were closed.
    """

    lock_file_pointer = os.open(f"/tmp/instance_{label}.lock", os.O_WRONLY | os.O_CREAT)

    try:
        fcntl.lockf(lock_file_pointer, fcntl.LOCK_EX | fcntl.LOCK_NB)
        already_running = False
    except IOError:
        already_running = True

    return already_running

def on_event(i3, e):
    mutt_window_title = "Mutt Mail"
    cmd = 'sleep 5 ; wmctrl -r "' + mutt_window_title + '" -b remove,demands_attention'
    if (e.change == 'urgent' and e.container.ipc_data['window_properties']['title'] == mutt_window_title):
        os.system(cmd)


if not instance_already_running():
    # Create Connection object (used to send commands and subscribe to events)
    i3 = Connection()
    # Subscribe to WINDOW event (fires when a window property changes)
    i3.on(Event.WINDOW, on_event)
    # Start main loop and wait for events to come in
    i3.main()
